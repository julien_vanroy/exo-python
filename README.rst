===============================
comptermots
===============================


.. image:: https://img.shields.io/pypi/v/comptermots.svg
        :target: https://pypi.python.org/pypi/comptermots

.. image:: https://img.shields.io/travis/gauthierc/comptermots.svg
        :target: https://travis-ci.org/gauthierc/comptermots

.. image:: https://readthedocs.org/projects/comptermots/badge/?version=latest
        :target: https://comptermots.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/gauthierc/comptermots/shield.svg
     :target: https://pyup.io/repos/github/gauthierc/comptermots/
     :alt: Updates


Exercice CompterMots dans une chaine de caractères.


* Free software: MIT license
* Documentation: https://comptermots.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

